package controladores;

import dao.ProductoDao;
import dao.RegistroDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import logica.Producto;
import logica.Registro;
import vistas.VistaAgregarProducto;
import vistas.VistaProductos;

public class AgregarProductoController implements ActionListener {

    private VistaAgregarProducto vAgregarProducto;
    private VistaProductos vProductos;

    public AgregarProductoController(VistaAgregarProducto vAgregarProducto, VistaProductos vProductos) {
        this.vAgregarProducto = vAgregarProducto;
        this.vProductos = vProductos;
        this.vAgregarProducto.getBtnGuardarr().addActionListener(this);
        this.vAgregarProducto.getBtnCancelar().addActionListener(this);
        this.vAgregarProducto.getjComboBoxCategoria().addActionListener(this);
    }

    public int insertarProducto(Producto p) {
        ProductoDao productoBD = new ProductoDao();
        productoBD.insertProducto(p);
        return productoBD.getUltimoProducto();
    }

    public void insertarRegistro(Registro r) {
        RegistroDao registroBD = new RegistroDao();
        registroBD.insertRegistro(r);
    }
    
    public String fechaActual(){
        java.util.Date fecha = new Date();
        return fecha+"";
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (this.vAgregarProducto.getBtnGuardarr() == e.getSource()) {
            Producto p;
            String nombre = vAgregarProducto.getTfNombre().getText();
            String descripcion = vAgregarProducto.getTfDescripcion().getText();
            int cantidad = Integer.parseInt(vAgregarProducto.getTfCantidad().getText());
            int categoria_id = (vAgregarProducto.getjComboBoxCategoria().getSelectedIndex() + 1);
            p = new Producto(nombre, descripcion, cantidad, categoria_id);
            
            int idProducto = insertarProducto(p);
            int idUsuario = vProductos.getvInicio().getVLogin().UsuarioLogeado().getId();
            Registro r = new Registro(fechaActual(), idProducto, idUsuario, 1, 0);
            insertarRegistro(r);
            
            this.vProductos.productosBD();
            this.vProductos.setVisible(true);
            this.vAgregarProducto.dispose();
        } else if (this.vAgregarProducto.getBtnCancelar() == e.getSource()) {
            this.vProductos.setVisible(true);
            this.vAgregarProducto.dispose();
        }
    }

}
