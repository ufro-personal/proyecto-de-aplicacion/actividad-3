package controladores;

import dao.CategoriaDao;
import dao.ProductoDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.Categoria;
import logica.Producto;
import vistas.VistaAgregarProducto;
import vistas.VistaCambiarCantidadProducto;
import vistas.VistaInicio;
import vistas.VistaLogin;
import vistas.VistaProductos;

public class ProductosController implements ActionListener {

    private VistaProductos vProductos;
    private VistaInicio vInicio;

    public ProductosController(VistaProductos vProductos, VistaInicio vInicio) {
        this.vProductos = vProductos;
        this.vInicio = vInicio;
        this.vProductos.getBtnAgregar().addActionListener(this);
        this.vProductos.getBtnCambiarCantidad().addActionListener(this);
    }

    public ArrayList<Producto> productos() {
        DefaultTableModel dtf = (DefaultTableModel) vProductos.getjTable1().getModel();
        ProductoDao productosBD = new ProductoDao();
        CategoriaDao categoriaBD = new CategoriaDao();
        ArrayList<Producto> productos = productosBD.getProuctos();
        dtf.setRowCount(0);
        for (int i = 0; i < productos.size(); i++) {
            String[] fila = {productos.get(i).getNombre(), productos.get(i).getDescripcion(), Integer.toString(productos.get(i).getCantidad()), categoriaBD.categoriaById(productos.get(i).getCategoria_id()).getNombre()};
            dtf.addRow(fila);
        }
        return productos;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.vProductos.getBtnAgregar() == e.getSource()) {
            CategoriaDao categoriasBD = new CategoriaDao();
            ArrayList<String> c = categoriasBD.getCategorias();
            String[] categorias = c.toArray(new String[c.size()]);

            VistaAgregarProducto v = new VistaAgregarProducto(categorias, vProductos);
            this.vProductos.setVisible(false);
            v.setVisible(true);
        } else if (this.vProductos.getBtnCambiarCantidad() == e.getSource()) {

            try {
                int posicion = this.vProductos.getjTable1().getSelectedRow();

                VistaCambiarCantidadProducto v = new VistaCambiarCantidadProducto(vProductos, posicion);
                this.vProductos.setVisible(false);
                v.setVisible(true);
            } catch (Exception ee) {
                JOptionPane.showMessageDialog(null, "Primero seleccione un producto de la tabla");
            }

        }
    }
}
