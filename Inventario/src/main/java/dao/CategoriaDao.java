
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import logica.Categoria;
import logica.Usuario;


public class CategoriaDao {
    private Conexion con;
    
    public CategoriaDao() {
        this.con = new Conexion();
    }
    
    public Categoria categoriaById(int idCategoria){
        Categoria c;
        Connection accesoBD=con.getConexion();
        String sql = "SELECT * FROM categoria WHERE id="+idCategoria;
        
        try {
            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id=rs.getInt("id");
                String nombre=rs.getString("nombre");
                c=new Categoria(id,nombre);
                return c;
            }else{
                return null;
            }
            
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener usuario");
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<String> getCategorias() {
        ArrayList<String> categoriasBD = new ArrayList<>();
        String c;
        Connection accesoBD = con.getConexion();

        try {
            String sql = "SELECT * FROM categoria";

            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet resultados = st.executeQuery(sql);

            while (resultados.next()) {
                String nombre = resultados.getString("nombre");
                c=nombre;
                categoriasBD.add(c);
            }
            return categoriasBD;
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener");
            e.printStackTrace();
            return null;
        }
    }
}
