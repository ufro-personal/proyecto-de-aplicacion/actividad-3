package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    
    Connection con=null;
    String user="root";// User de BD
    String pass="";// Pass de BD
    String server="jdbc:mysql://localhost:3306/"; //URL de Servidor que aloja la BD 
    String db="inventarioproductos"; //nombre de la BD
    String driver="com.mysql.jdbc.Driver";
    
    public Conexion() {
        
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(server+db, user, pass);
            System.out.println("Se ha iniciado la conexión con el servidor de forma exitosa");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
    }
    
    public Connection getConexion(){
        return con;   
    }

    public Connection cerrarConexion(){
        
        try {
            con.close();
             System.out.println("Cerrando conexion a "+server+db+" . . . . . Ok");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        con=null;
        return con;

    }
    
}
