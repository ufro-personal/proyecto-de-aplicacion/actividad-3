
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import logica.Producto;

public class ProductoDao {

    private Conexion con;
    
    public ProductoDao() {
        this.con = new Conexion();
    }
    
    public ArrayList<Producto> getProuctos() {
        ArrayList<Producto> productosBD = new ArrayList<>();
        Producto p;
        Connection accesoBD = con.getConexion();

        try {
            String sql = "SELECT * FROM producto";

            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet resultados = st.executeQuery(sql);

            while (resultados.next()) {
                int id = resultados.getInt("id");
                String nombre = resultados.getString("nombre");
                String descripcion = resultados.getString("descripcion");
                int cantidad = resultados.getInt("cantidad");
                int categoria_id = resultados.getInt("categoria_id");
                p=new Producto(id, nombre, descripcion, cantidad, categoria_id);
                productosBD.add(p);
            }
            return productosBD;
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener");
            e.printStackTrace();
            return null;
        }
    }
    
    public Producto getProductoById(int idProducto){
        Producto p;
        Connection accesoBD=con.getConexion();
        String sql = "SELECT * FROM producto WHERE id="+idProducto;
        
        try {
            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id = rs.getInt("id");
                String nombre=rs.getString("nombre");
                p=new Producto();
                p.setId(id);
                p.setNombre(nombre);
                return p;
            }else{
                return null;
            }
            
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener usuario");
            e.printStackTrace();
            return null;
        }
        
    }
    
    public void insertProducto(Producto p){
        
        Connection accesoBD=con.getConexion();
        
        try{
            String sql="INSERT INTO producto (nombre,descripcion,cantidad,categoria_id)" 
                        + "VALUES ('"+p.getNombre()+"','"+p.getDescripcion()
                        +"',"+p.getCantidad()+","+p.getCategoria_id()+")";
            
            Statement st=accesoBD.createStatement();
            st.executeUpdate(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    public int getUltimoProducto(){
        Connection accesoBD=con.getConexion();
        try{
            String sql="SELECT id FROM producto ORDER BY id DESC LIMIT 1";
            
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id = rs.getInt("id");
                return id;
            }
            return 0;
        }catch(Exception e){
            e.printStackTrace();
            return 0;
        }
        
    }
    
    public void alterarCantidad(int cantidadNueva,int id){
        
        Connection accesoBD=con.getConexion();
        
        try{
            String sql="UPDATE producto SET cantidad="+cantidadNueva+" WHERE id="+id;
            
            Statement st=accesoBD.createStatement();
            st.executeUpdate(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
}
