
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import logica.Producto;
import logica.Registro;

public class RegistroDao {
    private Conexion con;
    
    public RegistroDao() {
        this.con = new Conexion();
    }
    
    public ArrayList<Registro> getRegistros() {
        ArrayList<Registro> registrosBD = new ArrayList<>();
        Registro r;
        Connection accesoBD = con.getConexion();

        try {
            String sql = "SELECT * FROM registro";

            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet resultados = st.executeQuery(sql);

            while (resultados.next()) {
                int id = resultados.getInt("id");
                String fecha = resultados.getString("fecha");
                int producto_id = resultados.getInt("producto_id");
                int usuario_id = resultados.getInt("usuario_id");
                int accion_id = resultados.getInt("accion_id");
                int cantidad = resultados.getInt("cantidad");
                r=new Registro(id, fecha, producto_id, usuario_id, accion_id,cantidad);
                registrosBD.add(r);
            }
            return registrosBD;
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener");
            e.printStackTrace();
            return null;
        }
    }
    
    public void insertRegistro(Registro r){
        
        Connection accesoBD=con.getConexion();
        
        try{
            String sql="INSERT INTO registro (fecha,producto_id,usuario_id,accion_id,cantidad)" 
                        + "VALUES ('"+r.getFecha()+"',"+r.getProducto_id()
                        +","+r.getUsuario_id()+","+r.getAccion_id()+","+r.getCantidad()+")";
            
            Statement st=accesoBD.createStatement();
            st.executeUpdate(sql);
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
