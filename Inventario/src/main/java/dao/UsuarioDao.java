
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import logica.Usuario;

public class UsuarioDao {
    private Conexion con;
    
    public UsuarioDao() {
        this.con = new Conexion();
    }
    
    public Usuario getUsuarioByUser(String correo){
        Usuario u;
        Connection accesoBD=con.getConexion();
        String sql = "SELECT * FROM usuario WHERE correo='"+correo+"'";
        
        try {
            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id=rs.getInt("id");
                String nombre=rs.getString("nombre");
                String clave=rs.getString("clave");
                u=new Usuario(id,nombre,correo,clave);
                return u;
            }else{
                return null;
            }
            
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener usuario");
            e.printStackTrace();
            return null;
        }
        
    }
    
    public Usuario getUsuarioById(int idUsuario){
        Usuario u;
        Connection accesoBD=con.getConexion();
        String sql = "SELECT * FROM usuario WHERE id="+idUsuario;
        
        try {
            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id=rs.getInt("id");
                String nombre=rs.getString("nombre");
                String correo=rs.getString("correo");
                String clave=rs.getString("clave");
                u=new Usuario(id,nombre,correo,clave);
                return u;
            }else{
                return null;
            }
            
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener usuario");
            e.printStackTrace();
            return null;
        }
        
    }
}
