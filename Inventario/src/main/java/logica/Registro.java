
package logica;

public class Registro {
    private int id;
    private String fecha;
    private int producto_id;
    private int usuario_id;
    private int accion_id;
    private int cantidad;

    public Registro() {
    }

    public Registro(int id, String fecha, int producto_id, int usuario_id, int accion_id, int cantidad) {
        this.id = id;
        this.fecha = fecha;
        this.producto_id = producto_id;
        this.usuario_id = usuario_id;
        this.accion_id = accion_id;
        this.cantidad = cantidad;
    }

    public Registro(String fecha, int producto_id, int usuario_id, int accion_id, int cantidad) {
        this.fecha = fecha;
        this.producto_id = producto_id;
        this.usuario_id = usuario_id;
        this.accion_id = accion_id;
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(int producto_id) {
        this.producto_id = producto_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public int getAccion_id() {
        return accion_id;
    }

    public void setAccion_id(int accion_id) {
        this.accion_id = accion_id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    
    
    
}
