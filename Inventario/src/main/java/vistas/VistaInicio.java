
package vistas;

import controladores.InicioController;
import javax.swing.JButton;
import javax.swing.JFrame;

public class VistaInicio extends JFrame {

    private javax.swing.JButton btnIniciar;
    private javax.swing.JButton btnProductos;
    private javax.swing.JButton btnRegistros;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton btnCerrar;
    private VistaLogin vLogin;
    
    public VistaInicio(VistaLogin vLogin) {
         
        this.vLogin=vLogin;
        btnIniciar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnProductos = new javax.swing.JButton();
        btnRegistros = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();

        btnIniciar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnIniciar.setText("Iniciar sesion");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setText("Bienvenio a su inventario");

        btnProductos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnProductos.setText("Ver Productos");

        btnRegistros.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnRegistros.setText("Ver Registros");
        btnRegistros.setPreferredSize(new java.awt.Dimension(97, 25));
        
        btnCerrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnCerrar.setText("Cerrar sesion");
        btnCerrar.setPreferredSize(new java.awt.Dimension(97, 25));
        this.btnCerrar.addActionListener((e) -> {
            VistaLogin v=new VistaLogin();
            this.dispose();
            this.vLogin.dispose(); 
            v.setVisible(true);
        });

        InicioController c=new InicioController(this, vLogin);
        posicionar();
    }
    
    

    public JButton getBtnProductos() {
        return btnProductos;
    }

    public void setBtnProductos(JButton btnProductos) {
        this.btnProductos = btnProductos;
    }

    public JButton getBtnRegistros() {
        return btnRegistros;
    }

    public void setBtnRegistros(JButton btnRegistros) {
        this.btnRegistros = btnRegistros;
    }
         
    public VistaLogin getVLogin() {
        return vLogin;
    }

    public void setVLogin(VistaLogin vLogin) {
        this.vLogin = vLogin;
    }

    private void posicionar() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(60, 60, 60))
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCerrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnProductos)
                    .addComponent(btnRegistros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
        setResizable(false);
    }
    
}
