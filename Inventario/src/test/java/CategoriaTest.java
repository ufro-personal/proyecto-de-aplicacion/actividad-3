import dao.CategoriaDao;
import dao.ProductoDao;
import logica.Categoria;
import logica.Producto;
import org.junit.jupiter.api.*;

import java.util.List;

public class CategoriaTest {
    private Categoria categoria;
    private CategoriaDao categoriaDao;

    @BeforeEach
    public void init(){
        categoria = new Categoria();
        categoriaDao = new CategoriaDao();
    }

    @Test
    @DisplayName("buscar categoria por id")
    public void buscarCategoriaPorIdTest() {

        String valorEsperado = "frutas";
        String valorObtenido = categoriaDao.categoriaById(1).getNombre();

        Assertions.assertEquals(valorEsperado, valorObtenido);
        Assertions.assertNotEquals("pera", valorObtenido);
        Assertions.assertNotNull(categoriaDao.categoriaById(1));
    }
}
