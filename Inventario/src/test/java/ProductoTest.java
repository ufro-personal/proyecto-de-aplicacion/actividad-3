import dao.ProductoDao;
import logica.Producto;
import org.junit.jupiter.api.*;

import java.util.List;


public class ProductoTest {
    private Producto producto;
    private ProductoDao productoDao;

    @BeforeEach
    public void init(){
        producto = new Producto();
        productoDao = new ProductoDao();
    }

    @Test
    @DisplayName("agregar producto")
    public void agregarProductoTest() {
        producto.setNombre("pera");
        producto.setCantidad(20);
        producto.setDescripcion("fruta");
        producto.setCategoria_id(1);

        productoDao.insertProducto(producto);

        List<Producto> productos = productoDao.getProuctos();

        String valorEsperado = "pera";
        String valorObtenido = productos.get(productos.size()-1).getNombre();

        Assertions.assertEquals(valorEsperado, valorObtenido);
        Assertions.assertNotEquals("tomate", valorObtenido);
        Assertions.assertNotNull(productos.get(productos.size()-1));
    }

    @Test
    @DisplayName("buscar producto por id")
    public void buscarProductoPorIdTest() {

        String valorEsperado = "tomate";
        String valorObtenido = productoDao.getProductoById(57).getNombre();

        Assertions.assertEquals(valorEsperado, valorObtenido);
        Assertions.assertNotEquals("pera", valorObtenido);
        Assertions.assertNotNull(productoDao.getProductoById(57));
    }

    @Test
    @DisplayName("listar productos")
    public void listarProductosTest() {

        List<Producto> productos = productoDao.getProuctos();

        int valorEsperado = 3;
        int valorObtenido = productos.size();

        Assertions.assertEquals(valorEsperado, valorObtenido);
        Assertions.assertNotEquals(5, valorObtenido);
    }

}
