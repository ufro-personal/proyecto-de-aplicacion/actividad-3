import dao.CategoriaDao;
import dao.UsuarioDao;
import logica.Categoria;
import logica.Usuario;
import org.junit.jupiter.api.*;

public class UsuarioTest {

    private Usuario usuario;
    private UsuarioDao usuarioDao;

    @BeforeEach
    public void init(){
        usuario = new Usuario();
        usuarioDao = new UsuarioDao();
    }

    @Test
    @DisplayName("buscar usuario por id")
    public void buscarUsuarioPorIdTest() {

        String valorEsperado = "Usuario1";
        String valorObtenido = usuarioDao.getUsuarioById(5).getNombre();

        Assertions.assertEquals(valorEsperado, valorObtenido);
        Assertions.assertNotEquals("Usuario2", valorObtenido);
        Assertions.assertNotNull(usuarioDao.getUsuarioById(5));
    }
}

