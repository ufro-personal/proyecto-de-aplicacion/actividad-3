package dci.ufro.cl.ejercicio1.modelo;

public class Horario {

	private int dia;
	private String hora;

	public Horario() {
		// TODO - implement Horario.Horario
		throw new UnsupportedOperationException();
	}

	public int getDia() {
		return this.dia;
	}

	/**
	 * 
	 * @param dia
	 */
	public void setDia(int dia) {
		this.dia = dia;
	}

	public String getHora() {
		return this.hora;
	}

	/**
	 * 
	 * @param hora
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

}