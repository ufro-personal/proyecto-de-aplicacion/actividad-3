package dci.ufro.cl.ejercicio2;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Contiene todo los productos del minimercado
 *
 * @author Diego Pincheira
 */
public class Mercado {

    /**
     * Declaracion de atributos
     */
    private List<Producto> productos;

    /**
     * Creacion del constructor vacio
     */
    public Mercado() {
        productos=new ArrayList<>();
    }
    /**
     * agrega productos a la lista
     * @return  retorna un true si la funcion agrega el producto a la lista
     */
    public boolean agregarProductosNuevos(){

        Scanner teclado=new Scanner(System.in);

        System.out.println("Ingrese nombre del producto");
        String nombre=teclado.next();
        System.out.println("Ingrese su descripcion");
        String desc=teclado.next();
        System.out.println("Ingrese su cantidad");
        int cantidad=teclado.nextInt();
        System.out.println("Ingrese su categoria");
        String catg=teclado.next();

        Producto p=new Producto(nombre,desc,cantidad,catg);

        return this.productos.add(p);
    }
    /**
     * se muestran los productos del minimercado
     */
    public void mostrarProductos(){

        for (Producto p:this.productos) {
            System.out.println("Nombre: "+p.getNombre());
            System.out.println("Descripcion: "+p.getDescripcion());
            System.out.println("Cantidad: "+p.getCantidad());
            System.out.println("Categoria: "+p.getCategoria());
            System.out.println("");
        }
    }

}
