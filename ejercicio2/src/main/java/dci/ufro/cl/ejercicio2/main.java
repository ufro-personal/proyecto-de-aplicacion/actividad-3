package dci.ufro.cl.ejercicio2;

/**
 *
 * @author Diego Pincheira
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Mercado p=new Mercado();
        p.agregarProductosNuevos();
        p.mostrarProductos();
    }
}
